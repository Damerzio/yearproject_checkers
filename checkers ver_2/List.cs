﻿using System;
//реалізовуємо цей інтерфейс, для того щоб виеористовувати перерахування з використанням ітераторів, java
internal interface Enumeration
{
    //перевіряємо чи є більше елементів для ітерації
    //true - якшо є більше 
    bool hasMoreElements();

    //повертає даний елемент і переміщує ітератор
    object nextElement();
}

//викидаємо, коли щось викличе наступний елемент коли він за межами колекції
internal class NoSuchElementException : Exception
{
}

//список вузлів
internal class ListNode
{
    internal ListNode prev, next;
    internal object value;

    public ListNode(object elem, ListNode prevNode, ListNode nextNode)
    {
        value = elem;
        prev = prevNode;
        next = nextNode;
    }
}

//вткористовуємо клас list , колекцію ...https://msdn.microsoft.com/ru-ru/library/ybcx56wz.aspx
internal class List
{
    private ListNode head;
    private ListNode tail;
    private int count;

    public List()
    {
        count = 0;
    }

    //додає елемент до голови списку
    //elem - елемент що додається
    public void push_front(object elem)
    {
        ListNode node = new ListNode(elem, null, head);

        if (head != null)
            head.prev = node;
        else
            tail = node;

        head = node;
        count++;
    }

    //додає елемент до хвосту списку
    //elem - елемент що додається
    public void push_back(object elem)
    {
        ListNode node = new ListNode(elem, tail, null);

        if (tail != null)
            tail.next = node;
        else
            head = node;

        tail = node;
        count++;
    }

    //удаляємо улемент з голови списку
    public object pop_front()
    {
        if (head == null)
            return null;

        ListNode node = head;
        head = head.next;

        if (head != null)
            head.prev = null;
        else
            tail = null;

        count--;
        return node.value;
    }

    //удаляємо елемент з хвосту списку
    public object pop_back()
    {
        if (tail == null)
            return null;

        ListNode node = tail;
        tail = tail.prev;

        if (tail != null)
            tail.next = null;
        else
            head = null;

        count--;
        return node.value;
    }

    //перевірка чи список порожній
    //true-якшо порожній
    public bool isEmpty()
    {
        return head == null;
    }


    //повертає значення кількості елементів списку
    public int length()
    {
        return count;
    }

    // Додає ще один список до хвоста списку 
    public void append(List other)
    {
        ListNode node = other.head;

        while (node != null)
        {
            push_back(node.value);
            node = node.next;
        }
    }

    //удаляємо всі елементи списку
    public void clear()
    {
        head = tail = null;
    }

    //Повертає елемент з голови списку без вилучення
    public object peek_head()
    {
        if (head != null)
            return head.value;
        else
            return null;
    }

    //Повертає елемент з хвоста списку без вилучення
    public object peek_tail()
    {
        if (tail != null)
            return tail.value;
        else
            return null;
    }


    //перевіряє чи існує даний елемент в списку
    //elem - шуканий елемент
    public bool has(object elem)
    {
        ListNode node = head;

        while (node != null && !node.value.Equals(elem))
            node = node.next;

        return node != null;
    }

    //копіюємо список
    public object clone()
    {
        List temp = new List();
        ListNode node = head;

        while (node != null)
        {
            //temp.push_back (node.value.clone ());
            temp.push_back(node.value);
            node = node.next;
        }

        return temp;
    }

    //створюємо string подання списку у формі [elem1,elem2]...
    public override string ToString()
    {
        string temp = "[";
        ListNode node = head;

        while (node != null)
        {
            temp += node.value.ToString();
            node = node.next;
            if (node != null)
                temp += ", ";
        }
        temp += "]";

        return temp;
    }

    //ітератор списку
    class Enum : Enumeration
    {
        // даний елемент
        private ListNode node;

        internal Enum(ListNode start)
        {
            node = start;
        }

        //перевірка чи є більше елементів для перебору
        //true - якщо да
        public bool hasMoreElements()
        {
            return node != null;
        }
        //Повертає поточний елемент і переміщує ітератор
        public object nextElement()
        {
            Object temp;

            if (node == null)
                throw new NoSuchElementException();

            temp = node.value;
            node = node.next;

            return temp;
        }
    }

    //повертає список ітератор
    public Enumeration elements()
    {
        return new Enum(head);
    }

}
