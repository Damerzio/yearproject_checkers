﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Resources;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Threading;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using System.Collections;

namespace checkers
{
    public partial class Checkers : Form
    {
    public Checkers()
        {
            InitializeComponent();
        }
    public struct cells //структора дошки
        {   
           public  string name; //Ім'я фігури
           public  string colorFigure;// Колір фігури
           public int positionX;//координата на якій стоїть шашка
           public int positionY;
           public string pos_colorFigure;//Колір клітинки на якій стоїть фігура
           
        };
    internal List selected; //вибрані клітинки в ходах
    public Checkers board;
    public static cells[,] map; //створюємо дошку
    public static Point position; //координати вибраної фігури  
    public static bool movePosibillity=false; //false - ходить неможна
    public static string player="Білий";// true - білий
  
        public void inputFigurs(bool black) //Розставляю фігури на полі
    { 
        if (black) //flag = true чорні 
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if ((j == 0) && (i % 2 != 0))
                        map[i, j].name = "Шашка";
                    map[i, j].colorFigure = "Чорний";
                }
                for (int j = 1; j < 2; j++)
                {
                    if ((j == 1) && (i % 2 == 0))
                        map[i, j].name = "Шашка";
                    map[i, j].colorFigure = "Чорний";
                }

                for (int j = 2; j < 3; j++)
                {
                    if ((j == 2) && (i % 2 != 0))
                        map[i, j].name = "Шашка";
                    map[i, j].colorFigure = "Чорний";
                }
            }
        }
        else//білі
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 5; j < 7; j++)
                {
                    if ((j == 5) && (i % 2 == 0))
                        map[i, j].name = "Шашка";
                    map[i, j].colorFigure = "Білий";
                }

                for (int j = 6; j < 8; j++)
                {
                    if ((j == 6) && (i % 2 != 0))
                        map[i, j].name = "Шашка";
                    map[i, j].colorFigure = "Білий";
                }

                for (int j = 7; j < 8; j++)
                {
                    if ((j == 7) && (i % 2 == 0))
                        map[i, j].name = "Шашка";
                    map[i, j].colorFigure = "Білий";
                }
            }
        }
    }
    public void start()//розстановка клітинок
        {
            GameLogic game = new GameLogic();
            map = new cells[8,8]; //матриця 8х8
            inputFigurs(true); //true- для чорних
            inputFigurs(false);//false - білих
            int count = 1; 
            int posX=0,posY=0; //початкове значення
            for (int y = 0; y < 8; y++) 
            {
                for(int x = 0; x < 8; x++)
                    {   if (count % 2 > 0)//непарне
                        map[x, y].pos_colorFigure = "White"; 
                    else
                        map[x, y].pos_colorFigure = "Black"; // парне, то чорні
                    count++; 
                    map[x, y].positionX = posX;
                    map[x, y].positionY = posY;
                    posX += 70; //Малюнки, що загружаються - це квадрати 70х70 тому +70
                    }
                count++;
                posY += 70;
                posX = 0; 
            }
            game.count_black = 0;
            game.count_white = 0;
        }
     public void drawing() // розстановка фігур
        {
            Graphics board = this.CheckersPanel.CreateGraphics();
            Bitmap white_check = new Bitmap(@"figurs\WhiteCell.bmp"); //завантажую картинку пустої клітинки
            Bitmap black_check = new Bitmap(@"figurs\BlackCell.bmp");
            Bitmap temp;//змінна для тимчасового зберігання завантажених картинок
            for (int y = 0; y < 8; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    if (map[x, y].name != null) //якшо name!= null то там стоїть шашка
                    {
                        temp = new Bitmap(@"figurs\" + map[x, y].colorFigure + map[x, y].name + map[x, y].pos_colorFigure + ".bmp");
                        board.DrawImage(temp, map[x, y].positionX, map[x, y].positionY); 
                        temp.Dispose();//звільнюю тимчасову змінну
                    }
                    else //порожня клітинка
                    {
                        if (map[x, y].pos_colorFigure == "White") 
                            board.DrawImage(white_check, map[x, y].positionX, map[x, y].positionY); 
                        else
                            board.DrawImage(black_check, map[x, y].positionX, map[x, y].positionY);
                    }
                 
                }      
            }
            white_check.Dispose(); 
            black_check.Dispose();
            board.Dispose();
     }
     
     public void title()
     {
         Text = "Checkers - " + player;
     }
     public void change_king() //тут міняємо шашку на дамку 
     {

         if (player == "Білий" && player != "Чорний")
         {
             if (map[position.X, position.Y].name == "Шашка")
             {
                 if (position.X == 7 && position.Y == 0)
                 {
                     map[7, 0].name = "Дамка";
                 }
                 else if (position.X == 5 && position.Y == 0)
                 {
                     map[5, 0].name = "Дамка";
                 }
                 else if (position.X == 3 && position.Y == 0)
                 {
                     map[3, 0].name = "Дамка";
                 }
                 else if (position.X == 1 && position.Y == 0)
                 {
                     map[1, 0].name = "Дамка";
                 }

             }
         }
         else if (player == "Чорний" && player != "Білий")
         {
             if (map[position.X, position.Y].name == "Шашка")
             {
                 if (position.X == 0 && position.Y == 7)
                 {
                     map[0, 7].name = "Дамка";
                 }
                 else if (position.X == 2 && position.Y == 7)
                 {
                     map[2, 7].name = "Дамка";
                 }
                 else if (position.X == 4 && position.Y == 7)
                 {
                     map[4, 7].name = "Дамка";
                 }
                 else if (position.X == 6 && position.Y == 7)
                 {
                     map[6, 7].name = "Дамка";
                 }
                 drawing();
             }
         }
         drawing();
         movePosibillity = true;  
     }
      public int check_Koordinate(int c) //координати клітинок в пікселях
        {               //переводимо їх в індекси масиву
                if (c < 70) return 0; //якшо c<30 то індекс 0
                if (c > 70 && c < 140) return 1;
                if (c > 140 && c < 210) return 2;
                if (c > 210 && c < 280) return 3;
                if (c > 280 && c < 350) return 4;
                if (c > 350 && c < 420) return 5;
                if (c > 420 && c < 490) return 6;
                if (c > 490 && c < 560) return 7;
                return -1;//Якщо координата виходить за межі, то -1
            
        }
        public void panel1_MouseClick(object sender, MouseEventArgs e)//при натисканні на клітинку
      {
          if (check_Koordinate(e.X) >= 0 && check_Koordinate(e.Y) >= 0)//перевірка чи не виходить координата за межі
            if (movePosibillity) //Якщо ні, то перевіряю, чи була вибрана фігура для ходу
            {
                    GameLogic game = new GameLogic();
                    game.move(check_Koordinate(e.X), check_Koordinate(e.Y));
                    game.attack(check_Koordinate(e.X), check_Koordinate(e.Y));
                    game.king(check_Koordinate(e.X), check_Koordinate(e.Y));
                    change_king();
                    title();
                    movePosibillity = false;
            }
            else
            {
                    if (map[check_Koordinate(e.X), check_Koordinate(e.Y)].name == null)
                        movePosibillity = false;//не можна рухати порожні клітинки
                    else
                    {
                        position.X = check_Koordinate(e.X);
                        position.Y = check_Koordinate(e.Y);
                        movePosibillity = true;//Запам'ятовуємо координати вибраної шашки, і ставимо її куди небудь
                    }
                }
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            drawing();
        }
        private void Form1_Activated(object sender, EventArgs e)
        {
            drawing();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            start();
            drawing();
            player = "Білий";

        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
            drawing();
        }
        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            drawing(); 
        }
        public void новаГраToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GameLogic game = new GameLogic();
            start();//Заповнюю захову дошку
            drawing();//виводжу поле
            player = "Білий";//встановлюю гравця, котрий буде ходити першим
            game.count_black = 0;
            game.count_white = 0;
        }
    
        private void зберегтиГруToolStripMenuItem_Click_1(object sender, EventArgs e)
        {

            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.InitialDirectory = Directory.GetCurrentDirectory();
            saveDlg.Filter = "Checker files (*.sav)|*.sav|All files (*.*)|*.*";
            saveDlg.FilterIndex = 1;
            saveDlg.RestoreDirectory = true;

            if (saveDlg.ShowDialog() == DialogResult.OK)
            {
                FileStream fs1 = new FileStream(saveDlg.FileName, FileMode.Create);

                StreamWriter myStream = new StreamWriter(fs1);
                if ((myStream != null))
                {
                    saveBoard(fs1);
                    myStream.Close();
                }
            }        

        }
        public void saveBoard(FileStream file)
        {
            try
            {
                IFormatter formatter = (IFormatter)new BinaryFormatter();
                formatter.Serialize(file, board);
            }
            catch
            {
                MessageBox.Show("Помилка збереження", "Помилка",
                                 MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void відкритиГруToolStripMenuItem_Click(object sender, EventArgs e)
        {

            OpenFileDialog openDlg = new OpenFileDialog();

            openDlg.InitialDirectory = Directory.GetCurrentDirectory();
            openDlg.Filter = "Checker files (*.sav)|*.sav|All files (*.*)|*.*";
            openDlg.FilterIndex = 1;
            openDlg.RestoreDirectory = true;

            if (openDlg.ShowDialog() == DialogResult.OK)
            {
                FileStream kl = new FileStream(openDlg.FileName, FileMode.Open);

                if ((kl != null))
                {
                    loadBoard(kl);
                    kl.Close();
                }
            }        
        }
        public void loadBoard(Stream file)
        {
            try
            {
                IFormatter formatter = (IFormatter)new BinaryFormatter();

                // очищуємо вибрані ходи
                selected.clear();
                Invalidate();
                reset();

                // Десеріалізуємо об’єкт в потік
                board = (Checkers)formatter.Deserialize(file);
            }
            catch
            {
                MessageBox.Show("Помилка завантаження", "Помилка",
                               MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        Stack boards;
        public void reset()
        {
            boards = new Stack();
        }

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }


       
    }
}