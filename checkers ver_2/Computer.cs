﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

internal class Computer
{
/*    //дошка яка використовується для компа
    private checkers.Checkers currentBoard;

    //колір шашок компа
    private int color;

    //Макс глибина використовувана в мін-макс алгоритмі
    private int maxDepth = 1;
    //Масив tableWeight
    private int[] tableWeight = { 100, 4, 4, 4, 
                                 4, 3, 3, 3,
                                 3, 2, 2, 4,
                                 4, 2, 1, 3,
                                 3, 1, 2, 4,
                                 4, 2, 2, 3,
                                 3, 3, 3, 4,
                                 4, 4, 4, 4};


    public Computer(checkers.Checkers gameBoard)
    {
        string comp = "Чорний";
        comp = checkers.Checkers.player;
        int BLACK = Convert.ToInt32(checkers.Checkers.player);
        currentBoard = gameBoard;
        color = BLACK;
    }
    //Дозволяє користувачу змінювати макс глибину мін-макс дерева(рівень)
    public int depth
    {
        get
        {
            return maxDepth;
        }
        set
        {
            maxDepth = value;
        }
    }
    //комп робить хід який він отримує
    public void play()
    {
            List moves = minimax(currentBoard);
            if (!moves.isEmpty())
                currentBoard.move(moves);
      
    }
    //встановлюємо дошку, що на ній грає комп
    //board - нова дошка
    public void setBoard(checkers.Checkers board)
    {
        currentBoard = board;
    }

    //якшо хід вірний
    //moves - список ходів 
    private bool mayPlay(List moves)
    {
        return !moves.isEmpty() && !((List)moves.peek_head()).isEmpty();
    }


    //описуємо мінімакс алгоритм для ходів компа
    //board - генеруємо на дошці ходи із стартової точки
    private List minimax(checkers.Checkers board)
    {
        List sucessors;
        List move, bestMove = null;
        checkers.Checkers nextBoard;
        int value, maxValue = Int32.MinValue;

        sucessors = board.legalMoves();
        while (mayPlay(sucessors))
        {
            move = (List)sucessors.pop_front();
            nextBoard = (checkers.Checkers)board.clone();

            Debug.WriteLine("******************************************************************");
            nextBoard.move(move);
            value = minMove(nextBoard, 1, maxValue, Int32.MaxValue);

            if (value > maxValue)
            {
                Debug.WriteLine("Max value : " + value + " at depth : 0");
                maxValue = value;
                bestMove = move;
            }
        }

        Debug.WriteLine("Move value selected : " + maxValue + " at depth : 0");

        return bestMove;
    }

    //здійснює хід з точчки зору Max гравця
    //board - дошка, яка виеор для стартової точки
    //depth - глибина в мінімакс дереві
    //alpha - значення альфа-каналу для альфа-бета відсічення
    //beta - значення beta-каналу для альфа-бета відсічення
    private int maxMove(checkers.Checkers board, int depth, int alpha, int beta)
    {
        if (cutOffTest(board, depth))
            return eval(board);


        List sucessors;
        List move;
        checkers.Checkers nextBoard;
        int value;

        Debug.WriteLine("Max node at depth : " + depth + " with alpha : " + alpha +
                            " beta : " + beta);

        sucessors = board.legalMoves();
        while (mayPlay(sucessors))
        {
            move = (List)sucessors.pop_front();
            nextBoard = (checkers.Checkers)board.clone();
            nextBoard.move(move);
            value = minMove(nextBoard, depth + 1, alpha, beta);

            if (value > alpha)
            {
                alpha = value;
                Debug.WriteLine("Max value : " + value + " at depth : " + depth);
            }

            if (alpha > beta)
            {
                Debug.WriteLine("Max value with prunning : " + beta + " at depth : " + depth);
                Debug.WriteLine(sucessors.length() + " sucessors left");
                return beta;
            }

        }

        Debug.WriteLine("Max value selected : " + alpha + " at depth : " + depth);
        return alpha;
    }

    //здійснює хід з точчки зору MIN гравця
    //board - дошка, яка виеор для стартової точки
    //depth - глибина в мінімакс дереві
    //alpha - значення альфа-каналу для альфа-бета відсічення
    //beta - значення beta-каналу для альфа-бета відсічення
    private int minMove(checkers.Checkers board, int depth, int alpha, int beta)
    {
        if (cutOffTest(board, depth))
            return eval(board);


        List sucessors;
        List move;
        checkers.Checkers nextBoard;
        int value;

        Debug.WriteLine("Min node at depth : " + depth + " with alpha : " + alpha +
                            " beta : " + beta);

        sucessors = (List)board.legalMoves();
        while (mayPlay(sucessors))
        {
            move = (List)sucessors.pop_front();
            nextBoard = (checkers.Checkers)board.clone();
            nextBoard.move(move);
            value = maxMove(nextBoard, depth + 1, alpha, beta);

            if (value < beta)
            {
                beta = value;
                Debug.WriteLine("Min value : " + value + " at depth : " + depth);
            }

            if (beta < alpha)
            {
                Debug.WriteLine("Min value with prunning : " + alpha + " at depth : " + depth);
                Debug.WriteLine(sucessors.length() + " sucessors left");
                return alpha;
            }
        }

        Debug.WriteLine("Min value selected : " + beta + " at depth : " + depth);
        return beta;
    }
    //оцінка сили поточного гравця
    //board - дошка на якій все відбувається
    private int eval(checkers.Checkers board)
    {
        int colorKing;
        int colorForce = 0;
        int enemyForce = 0;
        int piece;

        if (color == checkers.Checkers.WHITE)
            colorKing = checkers.Checkers.WHITE_KING;
        else
            colorKing = checkers.Checkers.BLACK_KING;

        try
        {
            for (int i = 0; i < 32; i++)
            {
                piece = board.getPiece(i);

                if (piece != checkers.Checkers.EMPTY)
                    if (piece == color || piece == colorKing)
                        colorForce += calculateValue(piece, i);
                    else
                        enemyForce += calculateValue(piece, i);
            }
        }
        catch (BadCoord bad)
        {
            Debug.WriteLine(bad.StackTrace);
            Application.Exit();
        }

        return colorForce - enemyForce;
    }

    //визначаємо силу шашки
    //piece - тип шашки
    //pos- позиція
    private int calculateValue(int piece, int pos)
    {
        int value;

        if (piece == checkers.Checkers.WHITE)  //Проста шашка
            if (pos >= 4 && pos <= 7)
                value = 7;
            else
                value = 5;
        else if (piece != checkers.Checkers.BLACK)//проста шашка
            if (pos >= 24 && pos <= 27)
                value = 7;
            else
                value = 5;
        else // дамка
            value = 10;

        return value * tableWeight[pos];
    }

    //перевіряємо чи game-tree може бути скорочена
    //board - дошка на якій оцінюємо
    //depth - глибина 
    //true - якшо дерево може бути скорочене
    private bool cutOffTest(checkers.Checkers board, int depth)
    {
        return depth > maxDepth || board.hasEnded();
    }
    */
}

