﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using System.Resources;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Threading;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;

namespace checkers
{
    class GameLogic:Checkers

    {     
        public void go(int x, int y) //переміщення шашок
        {

            if (map[position.X, position.Y].colorFigure == player) //дивимося чи гравець бере свою шашку
            {
                if (map[x, y].name != null) //б'ємо
                {
                        if (map[x, y].colorFigure != map[position.X, position.Y].colorFigure)
                        {
                            map[x, y].name = map[position.X, position.Y].name;
                            map[x, y].colorFigure = map[position.X, position.Y].colorFigure;
                            map[position.X, position.Y].name = null;
                            map[position.X, position.Y].colorFigure = null;
                            count_black++;
                        }
               
                   
                    
                }
                else //ідемо
                {
                    map[x, y].name = map[position.X, position.Y].name;
                    map[x, y].colorFigure = map[position.X, position.Y].colorFigure;
                    map[position.X, position.Y].name = null;
                    map[position.X, position.Y].colorFigure = null;
                }
                if (player == "Білий")

                    player = "Чорний";
                else
                    player = "Білий";
                drawing();
            }
        }
        public void maxmin(ref int max, ref int min, int x, int y, bool x_or_y)//мін макс допустимі координати
        {
            if (x_or_y)
            {
                if (position.X > x)
                {
                    min = x;
                    max = position.X;
                }
                else
                {
                    min = position.X + 1;
                    max = x;
                }
            }
            else
            {
                if (position.Y > y)
                {
                    min = y;
                    max = position.Y;
                }
                else
                {
                    min = position.Y + 1;
                    max = y;
                }

            }
        }

        public int king(int x, int y) //ходи дамки
        {
            bool move_end = true;
            bool move = false;
            int i;
            int min_y = 0, min_x = 0, max_y = 0, max_x = 0;
            if (player == "Білий")
            {
                if (map[position.X, position.Y].name == "Дамка")
                {
                    maxmin(ref max_x, ref min_x, x, y, true); //завантажуємо функцію щоб не виходити за межі
                    maxmin(ref max_y, ref min_y, x, y, false);
                    if (map[x, y].name != null)
                    {
                        move = true;
                        min_x++;
                        min_y++;
                    }
                    for (i = min_x; i < max_x; i++, min_y++)
                        if (map[i, min_y].name != null)
                        {
                            move_end = false;
                            break;
                        }
                    if (((position.X > x) && (position.Y < y)) && (move_end && move) || move_end && map[x, y].colorFigure == "Чорний" && map[x, y].colorFigure != "Білий" && map[x - 1, y + 1].colorFigure != "Білий" && map[x - 1, y + 1].colorFigure != "Чорний")
                    {

                        map[x, y].name = null;
                        //if (map[x, y].name == null) count_black++;
                        go(x - 1, y + 1);
                    }
                    if (((position.X > x) && (position.Y > y)) && (move_end && move) || move_end && map[x, y].colorFigure == "Чорний" && map[x, y].colorFigure != "Білий" && map[x - 1, y - 1].colorFigure != "Білий" && map[x - 1, y - 1].colorFigure != "Чорний")
                    {

                        map[x, y].name = null;
                        //if (map[x, y].name == null) count_black++;
                        go(x - 1, y - 1);
                    }
                    if (((position.X < x) && (position.Y < y)) && (move_end && move) || move_end && map[x, y].colorFigure == "Чорний" && map[x, y].colorFigure != "Білий" && map[x + 1, y + 1].colorFigure != "Білий" && map[x + 1, y + 1].colorFigure != "Чорний")
                    {
                        map[x, y].name = null;
                        //if (map[x, y].name == null) count_black++;
                        go(x + 1, y + 1);
                    }

                    if (((position.X < x) && (position.Y > y)) && (move_end && move) || move_end && map[x, y].colorFigure == "Чорний" && map[x, y].colorFigure != "Білий" && map[x + 1, y - 1].colorFigure != "Білий" && map[x + 1, y - 1].colorFigure != "Чорний")
                    {

                        map[x, y].name = null;
                        //if (map[x, y].name == null) count_black++;
                        go(x + 1, y - 1);
                    }
                    if ((move_end && move) || move_end)
                    {
                        go(x, y);
                    }
                    return 1;
                }

            }
            if (player == "Чорний")
            {
                if (map[position.X, position.Y].name == "Дамка")
                {
                    maxmin(ref max_x, ref min_x, x, y, true);
                    maxmin(ref max_y, ref min_y, x, y, false);
                    if (map[x, y].name != null)
                    {
                        move = true;
                        min_x++;
                        min_y++;
                    }
                    for (i = min_x; i < max_x; i++, min_y++)
                        if (map[i, min_y].name != null)
                        {
                            move_end = false;
                            break;
                        }
                    if (((position.X > x) && (position.Y < y)) && (move_end && move) || move_end && map[x, y].colorFigure == "Білий" && map[x, y].colorFigure != "Чорний")//&& map[x - 1, y + 1].colorFigure != "Білий" && map[x - 1, y + 1].colorFigure != "Чорний")
                    {

                        map[x, y].name = null;
                        //if (map[x, y].name == null) count_white++;
                        go(x - 1, y + 1);
                    }
                    if (((position.X > x) && (position.Y > y)) && (move_end && move) || move_end && map[x, y].colorFigure == "Білий" && map[x, y].colorFigure != "Чорний")//&& map[x - 1, y - 1].colorFigure != "Білий" && map[x - 1, y - 1].colorFigure != "Чорний")
                    {

                        map[x, y].name = null;
                        //if (map[x, y].name == null) count_white++;
                        go(x - 1, y - 1);
                    }
                    if (((position.X < x) && (position.Y < y)) && (move_end && move) || move_end && map[x, y].colorFigure == "Білий" && map[x, y].colorFigure != "Чорний")// && map[x + 1, y + 1].colorFigure != "Білий" && map[x + 1, y + 1].colorFigure != "Чорний")
                    {

                        map[x, y].name = null;
                        //if (map[x, y].name == null) count_white++;
                        go(x + 1, y + 1);
                    }
                    if (((position.X < x) && (position.Y > y)) && (move_end && move) || move_end && map[x, y].colorFigure == "Білий" && map[x, y].colorFigure != "Чорний")// && map[x + 1, y - 1].colorFigure != "Білий" && map[x + 1, y - 1].colorFigure != "Чорний")
                    {

                        map[x, y].name = null;
                        //if (map[x, y].name == null) count_white++;
                        go(x + 1, y - 1);
                    }
                    if ((move_end && move) || move_end)
                    {
                        go(x, y);
                    }
                    return 1;
                }
            }
            movePosibillity = true;
            drawing();
            return 1;
        }
        public int move(int x, int y)  //хід
        {

            if (player == "Білий")
            {
                if (map[position.X, position.Y].name == "Шашка")
                {
                    if (((position.X - x) == 1 && (position.Y - y) == 1) && map[x, y].name == null) //головна
                    {
                        go(x, y);
                        return 1;
                    }
                    else if (((position.X - x) == -1 && (position.Y - y) == 1) && map[x, y].name == null) //побічна
                    {
                        go(x, y);
                        return 1;
                    }
                }
                Debug.WriteLine("Білий хід");
            }
            if (player == "Чорний")
            {
                if (map[position.X, position.Y].name == "Шашка")
                {
                    if (((position.X - x) == -1 && (position.Y - y) == -1) && map[x, y].name == null && map[x, y].colorFigure != "Чорний") //головна
                    {
                        go(x, y);
                        return 1;
                    }
                    if (((position.X - x) == 1 && (position.Y - y) == -1) && map[x, y].name == null && map[x, y].colorFigure != "Чорний") //побічна
                    {
                        go(x, y);
                        return 1;
                    }
                }
                Debug.WriteLine("Чорний хід");
         
            }
            movePosibillity = true;
            drawing();
            return 0;
        }
        
        public int attack(int x, int y) //атака
        {

            if (player == "Білий")
            {
                if (map[position.X, position.Y].name == "Шашка")
                {

                    if (((position.X - x) == -1) && (position.Y - y) == 1 && map[x, y].name != null && map[x, y].colorFigure != "Білий" && map[x + 1, y - 1].colorFigure != "Чорний") //побічна
                    {
                        map[x, y].name = null;
                        //if (map[x, y].name == null) count_black++;
                        go(x + 1, y - 1);
                        return 1;

                    }
                    if (((position.X - x) == 1) && (position.Y - y) == 1 && map[x, y].name != null && map[x, y].colorFigure != "Білий" && map[x - 1, y - 1].colorFigure != "Чорний") //головна
                    {
                        map[x, y].name = null;
                        //if (map[x, y].name == null) count_black++;
                        go(x - 1, y - 1);
                        return 1;
                    }
                }
                
                Debug.WriteLine("Чорних збито - "+count_black);
            }

            if (player == "Чорний")
            {
                if (map[position.X, position.Y].name == "Шашка")
                {
                    if (((position.X - x) == -1) && (position.Y - y) == -1 && map[x, y].name != null && map[x, y].colorFigure != "Чорний" && map[x + 1, y + 1].colorFigure != "Білий") //головна
                    {
                        map[x, y].name = null;
                        if (map[x, y].name == null) count_white++;
                        go(x + 1, y + 1);


                        return 1;
                    }
                    else if (((position.X - x) == 1) && (position.Y - y) == -1 && map[x, y].name != null && map[x, y].colorFigure != "Чорний" && map[x - 1, y + 1].colorFigure != "Білий") //побічна
                    {
                        {
                            map[x, y].name = null;
                            if (map[x, y].name == null) count_white++;
                            go(x - 1, y + 1);
                            return 1;
                        }
                    }

                }

                Debug.WriteLine("Білих збито - " + count_white);
                
            }
            if (count_black == 12) MessageBox.Show("Білий гравець переміг!");
            else if (count_white == 12) MessageBox.Show("Чорний гравець переміг!");
            movePosibillity = true;
            drawing();


            return 0;
        }

        public int count_black = 0;
        public int count_white = 0;  
    }

}
